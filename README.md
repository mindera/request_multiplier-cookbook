# Request Multiplier cookbook

This cookbook installs, configures and launches request multiplier.

## Notes
If ssh key access is needed for git, configure these these attributes first:

* default['multiplier']['git']['use_ssh'] = true
* default['multiplier']['git']['ssh']['key'] = 'PUT PRIVATE KEY HERE'

## Attributes

 (See attributes/default.rb)

## Requirements

In order to run tests vagrant, virtualbox, ruby, rake, foodcritic and rubocop must be installed.


## Testing

Run linting and kitchen tests:

 > rake test

Prepare a VM and connect to it:

 > vagrant up

 > vagrant ssh

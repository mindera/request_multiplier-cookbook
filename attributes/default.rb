default['nodejs']['install_method'] = 'binary'
default['nodejs']['version'] = '4.2.4'
default['nodejs']['binary']['checksum'] = 'dcae0c0faf9841ef38953075e67ca477ef9d2ea7c14ac2221de2429813f83a62'

default['multiplier']['script'] = './bin/request-multiplier'
default['multiplier']['path'] = '/opt/request_multiplier'
default['multiplier']['args'] = ['-c', '/etc/request_multiplier.conf']
default['multiplier']['instances'] = 4
default['multiplier']['exec_mode'] = 'cluster_mode'

default['multiplier']['git']['url'] = 'git@bitbucket.org:mindera/request-multiplier.git'
default['multiplier']['git']['branch'] = 'master'
default['multiplier']['git']['use_ssh'] = true
default['multiplier']['git']['ssh']['key'] = ''

#
# Cookbook Name:: request_multiplier
# Recipe:: default
#
# Copyright (c) 2016 The Authors, All Rights Reserved.

include_recipe 'nodejs'
include_recipe 'nodejs::npm'
include_recipe 'pm2::default'

# Make sure pm2 is available in our PATH
link '/usr/local/bin/pm2' do
  to lazy { "/usr/local/nodejs-binary-#{node['nodejs']['version']}/bin/pm2" }
  # to lazy { `npm prefix -g` + '/bin/pm2' } <-- this is adding a '?' at the end of prefix WTF
  link_type :symbolic
end

# FIXME: fetch the app from nexus in future

package 'git'

file '/tmp/id_rsa' do
  content node['multiplier']['git']['ssh']['key']
  owner 'root'
  group 'root'
  mode '0600'
  action :create
  only_if { node['multiplier']['git']['use_ssh'] }
end

file '/tmp/git_ssh_wrapper.sh' do
  content "#!/bin/sh\nexec /usr/bin/ssh -o StrictHostKeyChecking=no -i /tmp/id_rsa \"$@\""
  owner 'root'
  group 'root'
  mode '0700'
  action :create
  only_if { node['multiplier']['git']['use_ssh'] }
end

git '/opt/request_multiplier' do
  repository node['multiplier']['git']['url']
  revision 'master'
  ssh_wrapper '/tmp/git_ssh_wrapper.sh' if node['multiplier']['git']['use_ssh']
  action :sync
end

file '/tmp/id_rsa' do
  action :delete
end

file '/tmp/git_ssh_wrapper.sh' do
  action :delete
end

# Install request-multiplier dependencies
nodejs_npm 'request_multiplier' do
  path node['multiplier']['path']
  json true
end

# Setup pm2 json file for request-multiplier and start it
pm2_application 'request_multiplier' do
  script node['multiplier']['script']
  cwd node['multiplier']['path']
  args node['multiplier']['args']
  instances node['multiplier']['instances']
  exec_mode node['multiplier']['exec_mode']
  action [:deploy, :start_or_restart]
end
